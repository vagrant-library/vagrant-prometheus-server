#!/bin/bash

PROMETHEUS_VERSION=2.0.0
# URL = sonar.devcircus.com
SERVER_SUBDOMAIN="grafana"
SERVER_DOMAIN="devcircus.com"

#
# Install Grafana
#
function installGrafana {
  # Install Grafana
  curl https://packagecloud.io/gpg.key | sudo apt-key add -
  add-apt-repository "deb https://packagecloud.io/grafana/stable/debian/ stretch main"
  apt-get update
  apt-get -y install grafana
}

#
# Configure Grafana as a system service
#
function setupGrafanaInit {
    # Setup the Grafana init
    systemctl start grafana-server
    systemctl enable grafana-server
}

#
# Configure the Nginx server
#
function configureNginx {
	# Import the certificates
	cp /vagrant/config/certs/* /etc/ssl/private
	chmod 400 /etc/ssl/private/$SERVER_DOMAIN-keypair.pem
	cp /etc/ssl/private/$SERVER_DOMAIN-crt.pem /usr/local/share/ca-certificates/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
	update-ca-certificates --verbose
	# Link the certs
	ln -s /etc/ssl/private/$SERVER_DOMAIN-keypair.pem /etc/ssl/private/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.key
	ln -s /etc/ssl/private/$SERVER_DOMAIN-crt.pem /etc/ssl/certs/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
}

#
# Restart the Nginx service
#
function restartNginx {
	# Restart nginx so it can pick up the new configuration
	service nginx restart
}

echo "Setup Grafana server"

installGrafana
setupGrafanaInit
configureNginx
restartNginx

echo "Grafana setup complete"
