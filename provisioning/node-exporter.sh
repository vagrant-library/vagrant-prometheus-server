#!/bin/bash

NODE_EXPORTER_VERSION=0.17.0

#
# Install the node exporter
#
function installPrometheusNodeExporter {
  # Prepare the system
  useradd --no-create-home --shell /bin/false node_exporter
  # Install Prometheus
  wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION}/node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64.tar.gz
  tar xvf node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64.tar.gz
  cp node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64/node_exporter /usr/local/bin
  chown node_exporter:node_exporter /usr/local/bin/node_exporter
  # Cleanup the system
  rm -rf node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64.tar.gz node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64
}

#
# Setup the node exporter as a system service
#
function setupPrometheusNodeExporterInit {
    # Setup the exporter init
    cp /vagrant/config/node-exporter/node-exporter.service /etc/systemd/system/node-exporter.service
    systemctl daemon-reload
    systemctl start node-exporter
    systemctl enable node-exporter
}

echo "Setup Prometheus Node Exporter server"

installPrometheusNodeExporter
setupPrometheusNodeExporterInit

echo "Prometheus Node Exporter setup complete"
