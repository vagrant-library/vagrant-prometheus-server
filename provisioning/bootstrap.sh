#!/bin/bash

set -o errexit

#
#
#
function installSoftwareBase {
  # Install software base
  sudo apt-get update
  sudo apt-get upgrade -y
}

#
#
#
function setupUtilities {
  # so the `locate` command works
  apt-get install -y mlocate
  updatedb
  apt-get install -y unzip zip curl wget
	apt-get install -y build-essential
}

echo "Provisioning the Server"

installSoftwareBase
setupUtilities

echo "Server Successfully Provisioned"
