#!/bin/bash

PROMETHEUS_VERSION=2.5.0
# URL = sonar.devcircus.com
SERVER_SUBDOMAIN="prometheus"
SERVER_DOMAIN="devcircus.com"

#
# Prepare the system
#
function prepareTheSystem {
	# Add the system user
  useradd --no-create-home --system --shell /bin/false prometheus
	# Create the prometheus fodler
  mkdir /etc/prometheus
  mkdir /var/lib/prometheus
	# Set the right permissions
  chown prometheus:prometheus /etc/prometheus
  chown prometheus:prometheus /var/lib/prometheus
}

#
# Install the Prometheus service
#
function installPrometheus {
  # Get Prometheus
  wget https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/prometheus-${PROMETHEUS_VERSION}.linux-amd64.tar.gz
  tar xvf prometheus-${PROMETHEUS_VERSION}.linux-amd64.tar.gz
  cp prometheus-${PROMETHEUS_VERSION}.linux-amd64/prometheus /usr/local/bin/
  cp prometheus-${PROMETHEUS_VERSION}.linux-amd64/promtool /usr/local/bin/
  chown prometheus:prometheus /usr/local/bin/prometheus
  chown prometheus:prometheus /usr/local/bin/promtool
  cp -r prometheus-${PROMETHEUS_VERSION}.linux-amd64/consoles /etc/prometheus
  cp -r prometheus-${PROMETHEUS_VERSION}.linux-amd64/console_libraries /etc/prometheus
  chown -R prometheus:prometheus /etc/prometheus/consoles
  chown -R prometheus:prometheus /etc/prometheus/console_libraries
  # Config Prometheus
  cp /vagrant/config/prometheus/prometheus.yml /etc/prometheus/prometheus.yml
}

#
# Configure the service startup
#
function setupPrometheusInit {
    # Setup the prometheus init
    cp /vagrant/config/prometheus/prometheus.service /etc/systemd/system/prometheus.service
    systemctl daemon-reload
    systemctl start prometheus
    systemctl enable prometheus
}

#
# Clean up the installation
#
function cleanupTheSystem {
	# Cleanup the system
  rm -rf prometheus-${PROMETHEUS_VERSION}.linux-amd64.tar.gz prometheus-${PROMETHEUS_VERSION}.linux-amd64
}

#
# Configure the Nginx server
#
function configureNginx {
	# Link the configuration files
	ln -fs /vagrant/config/nginx/sites-enabled /etc/nginx/
	# Import the certificates
	cp /vagrant/config/certs/* /etc/ssl/private
	chmod 400 /etc/ssl/private/$SERVER_DOMAIN-keypair.pem
	cp /etc/ssl/private/$SERVER_DOMAIN-crt.pem /usr/local/share/ca-certificates/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
	update-ca-certificates --verbose
	# Link the certs
	ln -s /etc/ssl/private/$SERVER_DOMAIN-keypair.pem /etc/ssl/private/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.key
	ln -s /etc/ssl/private/$SERVER_DOMAIN-crt.pem /etc/ssl/certs/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
}

#
# Restart the Nginx service
#
function restartNginx {
	# Restart nginx so it can pick up the new configuration
	service nginx restart
}

echo "Setup Prometheus server"

prepareTheSystem
installPrometheus
setupPrometheusInit
cleanupTheSystem
configureNginx
restartNginx

echo "Prometheus setup complete"
